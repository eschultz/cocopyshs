

![CocoPySHS](https://github.com/pyshs/cocopyshs/blob/main/img/cocopyshs.png)

## Séance 1 - 17/03/2022

Présentation de Tristan Salord **Pois-chiche et "Franken-Code" - Verrouillage technologique et alimentation durable: le cas des légumineuses**

> Pour cette première séance, Tristan est revenu sur la stratégie qu'il a développé pour rendre *vivantes* des données d'ingrédients afin d'éclairer l'usage des légumineuses dans l'alimentation. Pour cela, il a rendu visible l'ensemble des opérations nécessaires pour passer de l'idée générale à sa décomposition en problèmes pouvant donner lieu à automatisation. Il a aussi partagé des éclairages et des expériences de sa pratique.

- [Les slides de la présentation](https://github.com/pyshs/cocopyshs/blob/main/docs/seance1_Tristan_Salord.pdf)
- [Le résumé de l'intervention](https://github.com/pyshs/cocopyshs/blob/main/docs/resume_seance1.md)

